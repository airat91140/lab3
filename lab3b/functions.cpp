#include "functions.h"

using std::cin, std::cout, std::endl;

namespace console {
    template<class T>
    void get_num(T &number) {
        while (true) {
            cin >> number;
            if (cin.good()) {
                return;
            }
            cout << "You are wrong. Repeat, please." << endl;
            cin.clear();
            cin.ignore(32767, '\n');
        }
    }

    int dialog(const prog::vector<std::string> &msgs) {
        std::string errmsg;
        int rc;
        do {
            cout << errmsg;
            errmsg = "You are wrong. Repeat, please!\n";
            for (const auto &iter : msgs)
                cout << iter << endl;
            cout << "Make your choice: --> " << endl;
            get_num(rc);
        } while (rc < 0 || rc >= msgs.size());
        return rc;
    }

    int add_flat(House &house) {
        int number;
        cout << "Enter flat number:" << endl;
        get_num(number);

        double area;
        cout << "Enter flat area:" << endl;
        get_num(area);

        prog::vector<Resident *> residents;

        int answer;
        std::string question = "Do you want to add more residents? (0 - no, 1 - yes)";
        cout << "Enter renter" << endl;
        do {
            try {
                Resident *res = make_res();
                residents.push_back(res->clone());
                delete res;
                cout << question << endl;
                cin >> answer;
            }
            catch (std::exception &error) {
                cout << "Error: " << error.what() << endl;
                answer = 1;
            }
        } while (answer);
        try {
            Flat flat(number, area, residents);
            house.add_flat(flat);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int delete_flat(House &house) {
        cout << house << endl;
        cout << "Enter flat number" << endl;
        int number;
        get_num(number);
        try {
            house.delete_flat(number);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int choose(House &house) {
        cout << house << endl;
        cout << "Enter flat number" << endl;
        int number;
        get_num(number);
        Flat flat;
        try {
            flat = house.find_flat(number);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
            return 1;
        }
        while (int rc = console::dialog(console::FlatFunctions)) {
            if (!flatfptr[rc](flat))
                break;
            house.set_flat_sum(flat.get_number());
        }
        return 1;
    }

    int view(House &house) {
        cout << house << endl;
        return 1;
    }

    int save(House &house) {
        std::string filename;
        cout << "Enter filename" << endl;
        cin >> filename;
        try {
            house.write_file(filename);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int load(House &house) {
        std::string filename;
        cout << "Enter filename" << endl;
        cin >> filename;
        try {
            house.read_file(filename);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int number(House &house) {
        int number;
        cout << "Enter new number" << endl;
        get_num(number);
        try {
            house.set_number(number);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int street(House &house) {
        std::string street;
        cout << "Enter new street" << endl;
        cin >> street;
        try {
            house.set_street(street);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int cost(House &house) {
        int cost;
        cout << "Enter new meter cost" << endl;
        get_num(cost);
        try {
            house.set_meter_cost(cost);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int add_resident(Flat &flat) {
        try {
            auto resident = make_res();
            flat.add_resident(resident);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    Resident *make_res() {
        int answer = -1, birthdate, gender, status;
        float percent;
        std::string tmpstr;
        Resident *result;
        cout << "Should it be benefit or not? (0 - usual, 1 - benefit)" << endl;
        while (answer < 0 || answer > 1)
            get_num(answer);
        if (answer) { // Benefit
            Benefit_Resident res;

            cout << "Input name:" << endl;
            cin >> tmpstr;
            res.set_name(tmpstr);

            cout << "Enter Birthdate" << endl;
            get_num(birthdate);
            res.set_birthdate(birthdate);

            cout << "Should it be male or female? (0 - male, 1 - female)" << endl;
            get_num(gender);
            res.set_gender(gender ? female : male);

            cout << "Should it be working or jobless? (0 - working, 1 - jobless)" << endl;
            get_num(status);
            res.set_birthdate(status ? jobless : working);

            cout << "Input benefit:" << endl;
            cin >> tmpstr;
            res.set_benefit(tmpstr);

            cout << "Input percent:" << endl;
            get_num(percent);
            res.set_percent(percent);

            result = res.clone();
        } else { //Usual
            Resident res;

            cout << "Input name:" << endl;
            cin >> tmpstr;
            res.set_name(tmpstr);

            cout << "Enter Birthdate" << endl;
            get_num(birthdate);
            res.set_birthdate(birthdate);

            cout << "Should it be male or female? (0 - male, 1 - female)" << endl;
            get_num(gender);
            res.set_gender(gender ? female : male);

            cout << "Should it be working or jobless? (0 - working, 1 - jobless)" << endl;
            get_num(status);
            res.set_status(status ? jobless : working);

            result = res.clone();
        }
        return result;
    }

    int delete_resident(Flat &flat) {
        try {
            auto resident = make_res();
            flat.delete_resident(resident);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int view(Flat &flat) {
        cout << flat << endl;
        return 1;
    }

    int area(Flat &flat) {
        double area;
        cout << "Enter new area" << endl;
        get_num(area);
        try {
            flat.set_area(area);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int view(Resident *resident) {
        cout << *resident << endl;
        return 1;
    }

    int name(Resident *resident) {
        std::string name;
        cout << "Enter new name" << endl;
        cin >> name;
        try {
            resident->set_name(name);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int gender(Resident *resident) {
        int gender;
        cout << "Set new gender. (0 - male, 1 - female)" << endl;
        get_num(gender);
        resident->set_gender(gender ? female : male);
        return 1;
    }

    int status(Resident *resident) {
        int status;
        cout << "Set new status. (0 - working, 1 - jobless)" << endl;
        get_num(status);
        resident->set_status(status ? jobless : working);
        return 1;
    }

    int birthdate(Resident *resident) {
        int birthdate;
        cout << "Enter new birthdate" << endl;
        get_num(birthdate);
        try {
            resident->set_birthdate(birthdate);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int benefit(Resident *resident) {
        if (!resident->is_benefit()) {
            cout << "Error, it is not benefit resident" << endl;
            return 1;
        }
        std::string benefit;
        cout << "Enter new benefit" << endl;
        cin >> benefit;
        try {
            dynamic_cast<Benefit_Resident *>(resident)->set_benefit(benefit);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int percent(Resident *resident) {
        if (!resident->is_benefit()) {
            cout << "Error, it is not benefit resident" << endl;
            return 1;
        }
        float percent;
        cout << "Enter new percent" << endl;
        get_num(percent);
        try {
            dynamic_cast<Benefit_Resident *>(resident)->set_percent(percent);
        }
        catch (std::exception &error) {
            cout << "Error: " << error.what() << endl;
        }
        return 1;
    }

    int choose(Flat &flat) {
        int number;
        cout << flat << endl;
        cout << "Choose number of resident" << endl;
        get_num(number);
        Resident *resident;
        try {
            resident = flat.get_residents().at(number + 1);
        }
        catch (std::exception &error) {
            cout << "Error! " << error.what() << endl;
            return 1;
        }
        while (int rc = console::dialog(console::ResidentFunctions)) {
            if (!residentfptr[rc](resident))
                break;
        }
        return 1;
    }
}
