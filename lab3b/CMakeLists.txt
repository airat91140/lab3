cmake_minimum_required(VERSION 3.17)
project(lab3b)

set(CMAKE_CXX_STANDARD 20)

add_subdirectory(lib)
include_directories(lib)

add_executable(lab3b
        main.cpp
        functions.cpp
        functions.h
        )

target_link_libraries(lab3b Housing)