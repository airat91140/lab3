#include "House.h"
#include "functions.h"

int main() {
    House house;
    int rc;
    while ((rc = console::dialog(console::HouseFunctions))) {
        if (!console::housefptr[rc](house))
            break;
    }
    return 0;
}
