/**
 \file
 \brief Заголовочный файл с описанием класса обыкновенного жильца
 *
 * Данный файл содержит в себе обьявления основных методов класса, определяющего обыкновенного жильца
*/
#ifndef LAB3_RESIDENT_H
#define LAB3_RESIDENT_H

#include "vector.h"
#include <iostream>
#include <string>
#include <ctime>

///Перечислиние, определяющее пол жителя
enum Gender {
    male, ///< Указывает мужской пол
    female ///< Указывает женсеий пол
};

///Перечисление, определяющее статус жителя
enum Status {
    working, ///< Указывает статус имеющего работу
    jobless ///< Указывает статус безработного
};

///Класс определяющий обыкновеннго жителя дома.
class Resident {
    friend class House;

private:
    std::string name; ///< Имя жильца
    int birthdate; ///< Год рождения жильца
    Gender gender; ///< Пол Жильца
    Status status; ///< Статус Жильца

protected:

    /**
     * \brief Вспомогательная функция вывода
     * \param [in,out] out Поток в который надо вывести жителя
     * \returns Возвращает поток с информацией о жителе
     */
    virtual std::ostream &print(std::ostream &out);

public:
    /**
     * \brief Конструктор класса по умолчанию
     * \details Создает класс со значениями по умолчанию:
     * Имя: "",
     * Дата рождения: 0,
     * Пол: мужской,
     * Статус: работающий.
     */
    Resident() : birthdate(0), gender(male), status(working) {};

    /**
     * \brief Инициализируещий конструктор класса.
     * \details Создает класс с заданными параметрами.
     * \param [in] name Имя жильца.
     * \param [in] birthdate Год рождения жильца.
     * \param [in] gender Пол жильца.
     * \param [in] status Статус жильца.
     * \throws std::invalid_argument В случае передачи пустой строки в поле #name.
     * \throws std::invalid_argument В случае передачи в поле #birthdate неположительного значения,
     * или значения превышающего текущий год.
     */
    Resident(const std::string &name, int birthdate, Gender gender, Status status);

    /**
     * \brief Пергеруженный оператор вывода в заданный поток.
     * \details Выводит имя, год рождения, пол и статус жильца.
     * \param [out] out Поток, в который будет выведена информация о жильце.
     * \param [in] resident Житель, информацию о котором надо вывести.
     * \returns Возвращает поток с введенной информации о жильце.
     */
    friend std::ostream &operator<<(std::ostream &out, Resident &resident);

    /**
     * \brief Метод определяющий, является ли жилец льготным, или нет.
     * \returns Возвращает \p false для данного класса.
     */
    [[nodiscard]] inline virtual bool is_benefit() const { return false; }

    /**
     * \brief Перегруженный оператор равенства
     * \details Оператор равенства, для проверки, являются ли два жильца одинаковыми.
     * \returns Возвращает \p true, если два жильца одинаковы, и \p false в противном случае.
     */
    bool operator==(const Resident &) const;

    /**
     * \brief Геттер года рождения.
     * \returns Возвращает год рождения жильца типа \p int.
     */
    [[nodiscard]] inline int get_birthdate() const { return birthdate; };

    /**
     * \brief Сеттер года рождения жильца.
     * \param [in] birthdate Новый год рождения жильца.
     * \throws std::invalid_argument В случае передачи в поле #birthdate неположительного числа,
     * или числа, превышающего текущий год.
     */
    void set_birthdate(int birthdate);

    /**
     * \brief Геттер имени.
     * \returns Возвращает имя жильца типа \p std::string.
     */
    [[nodiscard]] inline std::string get_name() const { return name; };

    /**
     * \brief Сеттер имени жильца.
     * \param [in] name Новое имя жильца
     * \throws std::invalid_argument В случае передачи в поле #name пустой строки.
     */
    void set_name(const std::string &name);

    /**
     * \brief Геттер пола.
     * \returns Возвращает пол жильца типа #Gender.
     */
    [[nodiscard]] inline Gender get_gender() const { return gender; };

    /**
     * \brief Сеттер пола жильца.
     * \param [in] g Новый пол жильца.
     */
    inline void set_gender(Gender g) noexcept { gender = g; };

    /**
     * \brief Геттер статуса жильца.
     * \returns Возвращает статус жильца типа #Status.
     */
    [[nodiscard]] inline Status get_status() const { return status; };

    /**
     * \brief Сеттер статуса жильца.
     * \param [in] s Новый статус жильца.
     */
    inline void set_status(Status s) noexcept { status = s; };

    /**
     * \brief Метод для клонирования текущего обьекта.
     * \details Виртуальный метод, предназначенный для правильного копирования данных из указателя на базовый класс.
     * \returns Возвращает указатель на новый класс #Resident, скопированный с текущего.
     */
    [[nodiscard]] virtual Resident *clone() const;

    /**
     * \brief Геттер процента скидки.
     * \returns Возвращает 0.
     */
    [[nodiscard]] virtual float get_percent() const { return 0; };

    /**
     * \brief Деструктор.
     * \details Деструктор по-умолчанию.
     */
    virtual ~Resident() = default;
};


#endif //LAB3_RESIDENT_H
