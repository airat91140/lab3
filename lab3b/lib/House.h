/**
 \file
 \brief Заголовочный файл с описанием класса дома.
 *
 * Данный файл содержит в себе обьявления методов класса, определяющего дом.
*/
#ifndef LAB3_HOUSE_H
#define LAB3_HOUSE_H

#include <map>
#include <fstream>

#include "flat.h"

///Класс, определяющий дом.
class House {
private:
    std::map<int, Flat> table; ///< Таблица квартир дома. Ключ является номер, значение - квартира.
    std::string street; ///< Улица дома.
    int number; ///< Номер дома.
    int corpus; ///< Корпус дома.
    int meter_cost; ///< Цена за квадратный метр.
    std::map<int, int> person_cost; /// Таблица стоимости аренды на одного жильца в каждой квартире. Ключ - номер квартиры, значение - стоимость.

    /**
     * \brief Метод коррекции стоимости аренды.
     * \details Вспомогательный метод для восстановления корректной стоимости во всех квартирах после изменения стоимости жилья.
     */
    void fix_person_cost();

    /**
     * \brief Вычисление стоимости жилья на одного человека в заданной квартире.
     * @return Возвращает стоимость жилья на человека типа \p int
     */
    [[nodiscard]] inline int get_flat_sum(const Flat &) const;

public:
    /**
     * \brief Восстановление платы за квартиру.
     * \details Восстановление правильной платы за квартиру после добавления или удаления жителя.
     */
    void set_flat_sum(int number);

    /**
     * \brief Запись таблицы в файл.
     * \details Метод сохраняет класс в указанное имя файла с раширеним ".tb".
     * @param [in] filename Название файла.
     * \throws std::invalid_argument В случае какой-то беды с файлом.
     */
    void write_file(const std::string &filename);

    /**
     * \brief Чтение таблицы из файла.
     * \details Метод загружает класс из файла с указанным именем.
     * @param [in] filename Название файла.
     * \throws std::invalid_argument В случае какой-то беды с файлом.
     */
    void read_file(const std::string &filename);

    /**
     * \brief Конструктор по-умолчанию.
     * \details Создает класс со значениями по умолчанию:
     * Номер дома: 0,
     * Корпус: 0,
     * Стоимость метра: 0,
     * Таблица квартир: {},
     * Таблица стоимостей: {},
     * Улица: "".
     */
    House() : table(), number(0), corpus(0), meter_cost(0), person_cost() {};

    /**
     * \brief Геттер корпуса.
     * @return Возвращает номер корпуса типа \p int.
     */
    [[nodiscard]] inline int get_corpus() const { return corpus; }

    /**
     * \brief Сеттер корпуса.
     * @param [in] corpus номер корпуса
     * \throws std::invalid_argument В случае неположительного значения поля \p corpus.
     */
    void set_corpus(int corpus);

    /**
     * \brief Добавление квартиры.
     * \details Метод добавляет не пустую квартиру в дом, при условии не сопадения номера квартиру с уже существующей.
     * @param [in] flat Квартира, которую нужно добавить.
     * \throws std::invalid_argument В случае, если квартира оказалась пустой(Flat.empty() = true).
     * \throws std::invalid_argument В случае, если номер такой квартиры уже существует.
     */
    void add_flat(const Flat &flat);

    /**
     * \brief Удаление квартиры.
     * \details Метод удаляет квартиру в доме, по ее номеру.
     * @param [in] number Номер квартиры, которую надо удалить.
     * \throws std::invalid_argument В случае, если указанный номер квартиры не существует.
     */
    void delete_flat(int number);

    /**
     * \brief Поиск квартиры.
     * \details Поиск квартиры в доме по ее номеру.
     * @param [in] number Номер квартиры.
     * @return Возвращет ссылку на квартиру из таблицы.
     * \throws std::invalid_argument В случае, если указанный номер квартиры не существует.
     */
    Flat &find_flat(int number);

    /**
     * Перегруженный оператор вывода.
     * @param [in,out] out Поток, в который надо вывести информацию о доме.
     * @param [in] house Дом, который надо вывести в поток.
     * @return Возвращает поток с введенной информацией о доме.
     */
    friend std::ostream &operator<<(std::ostream & out, const House &house);

    /**
     * \brief Геттер улицы.
     * @return Возвращет название улицы типа \p std::string.
     */
    [[nodiscard]] inline std::string get_street() const { return street; };

    /**
     * \brief Сеттер улицы.
     * @param [in] street Название улицы.
     * \throws std::invalid_argument В случае передачи в поле \p street пустой строки.
     */
    void set_street(const std::string &street);

    /**
     * \brief Геттер номера дома.
     * @return Возвращает номер дома типа \p int.
     */
    [[nodiscard]] inline int get_number() const { return number; };

    /**
     * \brief Сеттер дома.
     * @param [in] number Номер дома.
     * \throws std::invalid_argument В случае передачи в поле \p number неположительного числа.
     */
    void set_number(int number);

    /**
     * Геттер стоимости квадратного метра.
     * @return Возвращает стоимость квадратного метра типа \p int.
     */
    [[nodiscard]] inline int get_meter_cost() const { return meter_cost; };

    /**
     * Сеттер стоимости квадратного метра дома.
     * @param [in] meter_cost Стоимость одного квадратного метра дома.
     * \throws std::invalid_argument В случае отрицательного значения поля \p meter_cost.
     */
    void set_meter_cost(int meter_cost);

    /**
     * Геттер таблицы стоимостей квартир на одного человека.
     * @return Возвращает таблицу \p std::map стоимостей квартир на одного человека. Ключем является номер квартиры,
     * значением - стоимость.
     */
    [[nodiscard]] inline const std::map<int, int> &get_person_cost() const { return person_cost; };

    /**
     * Геттер таблицы квартир.
     * @return Возвращает таблицу \p std::map квартир. Ключем является номер квартиры,
     * значением - Квартира типа #Flat.
     */
    [[nodiscard]] inline const std::map<int, Flat> &get_table() const { return table; };
};

#endif //LAB3_HOUSE_H
