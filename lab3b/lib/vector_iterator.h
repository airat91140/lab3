#ifndef VECTOR_ITERATOR_H
#define VECTOR_ITERATOR_H

namespace prog {
    /**
     * \brief Итератор для вектора.
     * @tparam arg_type Элемент, на который он ссылается.
     */
    template<class arg_type>
    class vector_iterator {
    private:
        arg_type *current_;///< Указатель на элемент, на который ссылается итератор.
    public:
        /**
         * \brief Инициализирующий конструктор.
         * @param [in] element Элемент, на который будет ссылаться итератор.
         */
        explicit vector_iterator(arg_type *element) : current_(element) {};

        /**
         * \brief Оператор равно двух итераторов.
         * @param other Другой итератор.
         * @return Возвращает \p true, если два итератор ссылаются на один объект и \p false в противном случае.
         */
        bool operator==(const vector_iterator &other) const { return this->current_ == other.current_; };

        /**
         * \brief Оператор не равно двух итераторов.
         * @param other Другой итератор.
         * @return Возвращает \p true, если два итератор ссылаются на разыне объекты и \p false в противном случае.
         */
        bool operator!=(const vector_iterator &other) const { return this->current_ != other.current_; }

        /// Оператор разыименовывания итератора.
        arg_type &operator*() const { return *current_; }

        /// Оператор разыменовывания для стрелочки.
        arg_type *operator->() const { return current_; }

        /// Префиксный инкримент.
        vector_iterator &operator++() {
            ++current_;
            return *this;
        }

        /// Постфиксный инкримент.
        vector_iterator operator++(int) { return vector_iterator<arg_type>(current_++); }

        /// Оператор перемещения итератора на заданное чсисло.
        vector_iterator operator+(int n) { return vector_iterator(current_ + n); }
    };
}

#endif // VECTOR_ITERATOR_H
