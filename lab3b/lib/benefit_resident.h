/**
 \file
 \brief Заголовочный файл с описанием класса льготного жильца
 *
 * Данный файл содержит в себе обьявления основных методов класса, определяющего льготного жильца
*/
#ifndef LAB3_BENEFIT_RESIDENT_H
#define LAB3_BENEFIT_RESIDENT_H

#include <string>
#include "resident.h"

///Класс определяющий льготного жителя дома.
class Benefit_Resident : public Resident {
    friend class House;

private:
    std::string benefit; ///< Тип льготы жильца.
    float percent; ///< Скидка жильца в процентах.

protected:

    /**
     * \brief Вспомогательная функция вывода.
     * \param [in, out] out Поток, в который надо вывести жителя.
     * \return Возвращает поток, с введеными данными жителями.
     */
    std::ostream &print(std::ostream &out) override;

public:
    /**
     * \brief Конструктор по-умолчанию
     * \details Создает обьект данного класса со значениями по умолчанию:
     * Имя: "",
     * Дата рождения: 0,
     * Пол: мужской,
     * Статус: работающий,
     * Процент: 0,
     * Тип льготы: "".
     */
    Benefit_Resident() : Resident(), percent(0) {};

    /**
     * \brief Инициализирующий конструктор.
     * \details Создает обьект данного класса со значениями, переданными в аргументах.
     * \param [in] name Имя жильца.
     * \param [in] birthdate Дата рождения жильца.
     * \param [in] gender Пол жильца.
     * \param [in] status Статус жильца.
     * \param [in] benefit Тип льготы.
     * \param [in] percent Процент скидки.
     * \throws std::invalid_argument В случае передачи в поле \p name пустой строки.
     * \throws std::invalid_argument В случае передачи в поле \p birthdate неположительного значения,
     * или значения, превышающего текущий год.
     * \throws std::invalid_argument В случае передачи в поле \p benefit пустой строки.
     * \throws std::invalid_argument В случае передачи в поле \p percent значения меньшего 0 или большего 100.
     */
    Benefit_Resident(const std::string &name, int birthdate, Gender gender, Status status, const std::string &benefit, float percent);

    /**
     * \brief Метод определения льготности жильца.
     * \return Возвращает \p true для данного класса.
     */
    [[nodiscard]] inline bool is_benefit() const override { return true; };

    /**
     * \brief Геттер процента скидки.
     * @return Возвращает значение процента скидки.
     */
    [[nodiscard]] float get_percent() const override { return percent; }

    /**
     * \brief Сеттер процента скидки.
     * \throws std::invalid_argument В случае передачи в поле \p percent значения меньшего 0 или большего 100.
     */
    void set_percent(float percent);

    /**
     * \brief Перегруженный оператор вывода.
     * @param [in, out] out Поток, в который надо вывести жителя.
     * @param [in] resident Житель, которого надо вывести.
     * @return Возвращает поток, с введенными жителем.
     */
    friend std::ostream &operator<<(std::ostream &out, Benefit_Resident &resident);

    /**
     * \brief Сеттер типа льготы.
     * @param [in] benefit Тип льготы.
     * \throws std::invalid_argument В случае передачи в поле \p benefit пустой строки.
     */
    void set_benefit(const std::string &benefit);

    /**
     * \brief Геттер типа льготы.
     * @return Возвращает тип льготы.
     */
    [[nodiscard]] inline std::string get_benefit() const { return benefit; }

    /**
     * \brief Метод для клонирования текущего обьекта.
     * \details Виртуальный метод, предназначенный для правильного копирования данных из указателя на базовый класс.
     * \returns Возвращает указатель на новый класс #Benefit_Resident, скопированный с текущего.
     */
    [[nodiscard]] Benefit_Resident *clone() const override;

};


#endif //LAB3_BENEFIT_RESIDENT_H
