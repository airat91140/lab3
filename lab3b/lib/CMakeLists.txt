project(lib)

add_library(Housing STATIC
        resident.cpp
        resident.h
        benefit_resident.cpp
        benefit_resident.h
        flat.cpp
        flat.h
        House.cpp
        House.h
        vector.h
        vector_iterator.h
        )
