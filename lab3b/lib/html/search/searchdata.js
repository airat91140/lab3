var indexSectionsWithContent =
{
  0: "abcdefghijmoprsvw~",
  1: "bfhrv",
  2: "p",
  3: "bfhrv",
  4: "abcdefghioprsvw~",
  5: "i",
  6: "gs",
  7: "fjmw",
  8: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related"
};

var indexSectionLabels =
{
  0: "Указатель",
  1: "Классы",
  2: "Пространства имен",
  3: "Файлы",
  4: "Функции",
  5: "Определения типов",
  6: "Перечисления",
  7: "Элементы перечислений",
  8: "Друзья"
};

