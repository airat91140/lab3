#ifndef LAB3B_FUNCTIONS_H
#define LAB3B_FUNCTIONS_H

#include "House.h"
#include "vector.h"

namespace console {
    const prog::vector<std::string> HouseFunctions = {"0. Exit",
                                                      "1. Add flat",
                                                      "2. Delete flat",
                                                      "3. Choose flat",
                                                      "4. View house information",
                                                      "5. Save in file",
                                                      "6. Open from file",
                                                      "7. Change house number",
                                                      "8. Change house street",
                                                      "9. Change meter cost"
    };

    const prog::vector<std::string> FlatFunctions = {"0. Exit",
                                                     "1. Add resident",
                                                     "2. Delete resident",
                                                     "3. View flat information",
                                                     "4. Change flat area",
                                                     "5. Choose Resident"
    };

    const prog::vector<std::string> ResidentFunctions = {"0. Exit",
                                                         "1. View resident information",
                                                         "2. Change resident name",
                                                         "3. Change resident gender",
                                                         "4. Change resident status",
                                                         "5. Change resident birthdate",
                                                         "6. Change resident benefit",
                                                         "7. Change resident percent"
    };

    template<class T>
    void get_num(T &);

    int dialog(const prog::vector<std::string> &);

    int add_flat(House &);

    int delete_flat(House &);

    int choose(House &);

    int view(House &);

    int save(House &);

    int load(House &);

    int number(House &);

    int street(House &);

    int cost(House &);

    int add_resident(Flat &);

    int delete_resident(Flat &);

    int view(Flat &);

    int area(Flat &);

    int choose(Flat &);

    int view(Resident *);

    int name(Resident *);

    int gender(Resident *);

    int status(Resident *);

    int birthdate(Resident *);

    int benefit(Resident *);

    int percent(Resident *);

    Resident *make_res();

    const static prog::vector<int (*)(House &)> housefptr({nullptr, add_flat, delete_flat, choose,
                                                           view, save, load, number, street, cost});

    const static prog::vector<int (*)(Flat &)> flatfptr({nullptr, add_resident, delete_resident,
                                                         view, area, choose});

    const static prog::vector<int (*)(Resident *)> residentfptr({nullptr, view, name,
                                                                 gender, status, birthdate, benefit, percent});
} // console

#endif //LAB3B_FUNCTIONS_H
