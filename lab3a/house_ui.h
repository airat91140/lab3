#ifndef HOUSE_UI_H
#define HOUSE_UI_H

#include <QDialog>
#include "House.h"

namespace Ui {
class house_ui;
}

class house_ui : public QDialog
{
    Q_OBJECT

public:
    explicit house_ui(QWidget *parent = nullptr);
    explicit house_ui(House &, QWidget *parent = nullptr);

    ~house_ui();

private slots:
    void on_SaveButton_clicked();

private:
    Ui::house_ui *ui;
    House *house;
};

#endif // HOUSE_UI_H
