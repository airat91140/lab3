#ifndef FLAT_NEW_H
#define FLAT_NEW_H

#include <QDialog>
#include "mainwindow.h"
#include "House.h"

namespace Ui {
class flat_new;
}

class flat_new : public QDialog
{
    Q_OBJECT

public:
    explicit flat_new(House *house = nullptr, MainWindow *parent = nullptr);

    MainWindow *parent;

    ~flat_new();

private slots:

    void on_AddButton_clicked();

private:
    Ui::flat_new *ui;
    House *house;
};

#endif // FLAT_NEW_H
