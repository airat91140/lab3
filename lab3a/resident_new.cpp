#include "resident_new.h"
#include "ui_resident_new.h"
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QListWidget>
#include <QMessageBox>

Resident_new::Resident_new(MainWindow *parent, House *house) :
    QDialog(parent),
    ui(new Ui::Resident_new),
    house(house),
    parent(parent)
{
    ui->setupUi(this);
}

Resident_new::~Resident_new()
{
    delete ui;
}

void Resident_new::on_SaveButton_clicked()
{
    try {
        Resident *resident;
        if (ui->BenefitComboBox->currentText() == "True") {
            Benefit_Resident res(ui->NameLineEdit->text().toStdString(), ui->BirthdateSpinBox->value(), ui->GenderComboBox->currentText() == "Male" ? male : female, ui->StatusComboBox->currentText() == "Working" ? working : jobless, ui->BenefitTypeLineEdit->text().toStdString(), ui->PercentDoubleSpinBox->value());
            resident = res.clone();
        }
        else {
            Resident res(ui->NameLineEdit->text().toStdString(), ui->BirthdateSpinBox->value(), ui->GenderComboBox->currentText() == "Male" ? male : female, ui->StatusComboBox->currentText() == "Working" ? working : jobless);
            resident = res.clone();
        }
        if (parent->ui->FlatListWidget->currentItem() == nullptr)
            throw std::invalid_argument("Choose flat first");
        house->find_flat(parent->ui->FlatListWidget->currentItem()->text().toInt()).add_resident(resident);
        delete resident;
        this->close();
        parent->ShowResidents();
        house->set_flat_sum(parent->ui->FlatListWidget->currentItem()->text().toInt());
        delete this;
    }
    catch (std::exception &exc) {
        QMessageBox alert(QMessageBox::Warning, "", exc.what());
        alert.exec();
    }
}
