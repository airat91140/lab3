#include <QDialog>
#include <QSpinBox>
#include <QMessageBox>
#include <QInputDialog>
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "house_ui.h"
#include "ui_house_ui.h"
#include "flat_new.h"
#include "ui_flat_new.h"
#include "flat_ui.h"
#include "ui_flat_ui.h"
#include "resident_new.h"
#include "ui_resident_new.h"

template<class arg_type>
class vector;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionInfo_triggered()
{
    auto dialog = new house_ui(house, this);
    dialog->open();
}

void MainWindow::on_AddFlatButton_clicked()
{
    auto dialog = new flat_new(&house, this);
    dialog->open();
}

void MainWindow::ShowFlats()
{
    ui->FlatListWidget->clear();
    auto table = house.get_table();
    for (const auto& iter : table) {
        ui->FlatListWidget->addItem(QString::number(iter.first));
    }
}

void MainWindow::ShowResidents()
{
    ui->ResidentListWidget->clear();
    if (ui->FlatListWidget->currentItem() == nullptr)
        return;
    Flat flat = house.find_flat(ui->FlatListWidget->currentItem()->text().toInt());
    ui->ResidentListWidget->clear();
    for (auto iter = flat.get_residents().begin(); iter != flat.get_residents().end(); ++iter)
        ui->ResidentListWidget->addItem(QString::fromStdString((*iter)->get_name()));
}

void MainWindow::on_FlatListWidget_itemClicked(QListWidgetItem *item)
{
    Flat flat = house.find_flat(item->text().toInt());
    ui->ResidentListWidget->clear();
    for (auto iter : flat.get_residents())
        ui->ResidentListWidget->addItem(QString::fromStdString(iter->get_name()));
}

void MainWindow::on_ResidentListWidget_clicked(const QModelIndex &index)
{
    Flat flat = house.find_flat(ui->FlatListWidget->currentItem()->text().toInt());
    Resident * resident = flat.get_residents()[index.row()];
    ui->BirthdateSpinBox->setValue(resident->get_birthdate());
    ui->NameLineEdit->setText(QString::fromStdString(resident->get_name()));
    ui->GenderComboBox->setCurrentIndex(resident->get_gender());
    ui->StatusComboBox->setCurrentIndex(resident->get_status());
    ui->PercentDoubleSpinBox->setValue(resident->get_percent());
    if (resident->is_benefit()) {
        ui->BenefitTypeLineEdit->setText(QString::fromStdString(dynamic_cast<Benefit_Resident *>(resident)->get_benefit()));
        ui->BenefitComboBox->setCurrentIndex(0);
    }
    else {
        ui->BenefitTypeLineEdit->setText("");
        ui->BenefitComboBox->setCurrentIndex(1);
    }
}

void MainWindow::on_AddResidentButton_clicked()
{
    auto dialog = new Resident_new(this, &house);
    dialog->open();
}

void MainWindow::on_SaveButton_clicked()
{
    try {
        if (ui->ResidentListWidget->currentItem() == nullptr)
            throw std::logic_error("Choose resident at first");
        Resident *res = house.find_flat(ui->FlatListWidget->currentItem()->text().toInt()).get_residents().at(ui->ResidentListWidget->currentRow());
        res->set_name(ui->NameLineEdit->text().toStdString());
        res->set_gender(ui->GenderComboBox->currentText() == "Male" ? male : female);
        res->set_birthdate(ui->BirthdateSpinBox->value());
        res->set_status(ui->StatusComboBox->currentText() == "Working" ? working : jobless);
        if (res->is_benefit()) {
            dynamic_cast<Benefit_Resident *>(res)->set_percent(ui->PercentDoubleSpinBox->value());
            dynamic_cast<Benefit_Resident *>(res)->set_benefit(ui->BenefitTypeLineEdit->text().toStdString());
        }
        ShowResidents();
    }
    catch (std::exception &exc) {
        QMessageBox alert(QMessageBox::Warning, "", exc.what());
        alert.exec();
    }
}

void MainWindow::on_DeleteButton_clicked()
{
    try {
        if (ui->ResidentListWidget->currentItem() == nullptr)
            throw std::logic_error("Choose resident at first");
        Resident *res = house.find_flat(ui->FlatListWidget->currentItem()->text().toInt()).get_residents().at(ui->ResidentListWidget->currentRow());
        house.find_flat(ui->FlatListWidget->currentItem()->text().toInt()).delete_resident(res);
        ShowResidents();
        house.set_flat_sum(ui->FlatListWidget->currentItem()->text().toInt());
    }
    catch (std::exception &exc) {
        QMessageBox alert(QMessageBox::Warning, "", exc.what());
        alert.exec();
    }
}

void MainWindow::on_FlatListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    auto dialog = new flat_ui(&house.find_flat(item->text().toInt()), this);
    dialog->open();
}

void MainWindow::on_actionSave_as_triggered()
{
    try {
        QString filename = QInputDialog::getText(this, "", "Enter filename", QLineEdit::Normal);
        if (filename.isEmpty())
             throw std::invalid_argument("Empty filename");
        house.write_file(filename.toStdString());
    }
    catch (std::exception &exc) {
       QMessageBox alert(QMessageBox::Warning, "", exc.what());
       alert.exec();
    }
}

void MainWindow::on_actionOpen_triggered()
{
    try {
        QString filename = QInputDialog::getText(this, "", "Enter filename", QLineEdit::Normal);
        if (filename.isEmpty())
             throw std::invalid_argument("Empty filename");
        house.read_file(filename.toStdString());
        ShowFlats();
    }
    catch (std::exception &exc) {
       QMessageBox alert(QMessageBox::Warning, "", exc.what());
       alert.exec();
    }
}
