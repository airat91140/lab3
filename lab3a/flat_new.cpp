#include "flat_new.h"
#include "ui_flat_new.h"
#include "House.h"

#include <QMessageBox>

flat_new::flat_new(House *house, MainWindow *parent) : QDialog(parent), parent(parent){
    ui = new Ui::flat_new;
    ui->setupUi(this);
    this->house = house;
}

flat_new::~flat_new()
{
    delete ui;
}

void flat_new::on_AddButton_clicked()
{
    try {
        Resident *resident;
        if (ui->BenefitComboBox->currentText() == "True") {
            Benefit_Resident res(ui->NameLineEdit->text().toStdString(), ui->BirthdateSpinBox->value(), ui->GenderComboBox->currentText() == "Male" ? male : female, ui->StatusComboBox->currentText() == "Working" ? working : jobless, ui->BenefitTypeLineEdit->text().toStdString(), ui->PercentDoubleSpinBox->value());
            resident = res.clone();
        }
        else {
            Resident res(ui->NameLineEdit->text().toStdString(), ui->BirthdateSpinBox->value(), ui->GenderComboBox->currentText() == "Male" ? male : female, ui->StatusComboBox->currentText() == "Working" ? working : jobless);
            resident = res.clone();
        }
        Flat flat(ui->NumberSpinBox->value(), ui->AreaSpinBox->value(), {resident});
        house->add_flat(flat);
        delete resident;
        this->close();
        parent->ShowFlats();
        delete this;
    }  catch (std::exception &exc) {
        QMessageBox alert(QMessageBox::Warning, "", exc.what());
        alert.exec();
    }
}
