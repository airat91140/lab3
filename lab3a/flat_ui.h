#ifndef FLAT_UI_H
#define FLAT_UI_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class flat_ui;
}

class flat_ui : public QDialog
{
    Q_OBJECT

public:
    explicit flat_ui(Flat *, MainWindow *parent = nullptr);

    ~flat_ui();

private slots:
    void on_pushButton_clicked();

    void on_DeleteButton_clicked();

private:
    Ui::flat_ui *ui;
    Flat *flat;
    MainWindow *parent;
};

#endif // FLAT_UI_H
