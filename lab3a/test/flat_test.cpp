#include "gtest/gtest.h"
#include "flat.h"

TEST(FlatConstructors, DefaultConstructor)
{
    Flat a;
    ASSERT_EQ(0, a.get_number());
    ASSERT_EQ(0, a.get_area());
    ASSERT_EQ(true, a.get_residents().empty());
}

TEST(FlatConstructors, InitConstructor)
{
    Resident usual_empty, usual_filled("Airat", 2001, male, jobless);
    Benefit_Resident benefit_empty, benefit_filled("Elena", 1971, female, working, "Student", 10);
    Flat a(10, 12, {&usual_empty, &usual_filled, &benefit_empty, &benefit_filled});
    ASSERT_EQ(10, a.get_number());
    ASSERT_EQ(12, a.get_area());
    ASSERT_EQ(usual_empty, *a.get_residents()[0]);
    ASSERT_EQ(usual_filled, *a.get_residents()[1]);
    ASSERT_EQ(benefit_empty, *a.get_residents()[2]);
    ASSERT_EQ(benefit_filled, *a.get_residents()[3]);
}

TEST(FlatConstructors, TestExceptions)
{
    Resident us_emp, us_fill("Airat", 2001, male, jobless);
    Benefit_Resident ben_emp, ben_fill("Elena", 1971, female, working, "Student", 10);
    ASSERT_THROW(Flat(-1, 12, {&us_emp, &us_fill, &ben_emp, &ben_fill}), std::invalid_argument);
    ASSERT_THROW(Flat(0, 12, {&us_emp, &us_fill, &ben_emp, &ben_fill}), std::invalid_argument);
    ASSERT_THROW(Flat(10, -1, {&us_emp, &us_fill, &ben_emp, &ben_fill}), std::invalid_argument);
    ASSERT_THROW(Flat(10, 0, {&us_emp, &us_fill, &ben_emp, &ben_fill}), std::invalid_argument);
    ASSERT_THROW(Flat(10, 12, {}), std::invalid_argument);
}

TEST(FlatFunctions, TestFunctions)
{
    Resident us_emp, us_fill("Airat", 2001, male, jobless);
    Benefit_Resident ben_emp, ben_fill("Elena", 1971, female, working, "Student", 10);
    Flat a, b(10, 12, {&us_emp, &us_fill, &ben_emp, &ben_fill});

    a.add_resident(&us_emp);
    ASSERT_EQ(us_emp, *a.get_residents().back());

    b.add_resident(&ben_fill);
    ASSERT_EQ(ben_fill, *b.get_residents().back());

    b.delete_resident(&us_fill);
    ASSERT_EQ(us_emp, *b.get_residents()[0]);
    ASSERT_EQ(ben_emp, *b.get_residents()[1]);
    ASSERT_EQ(ben_fill, *b.get_residents()[2]);
    ASSERT_EQ(ben_fill, *b.get_residents()[3]);
    ASSERT_EQ(4, b.get_residents_number());

    ASSERT_EQ(1, b.get_usual_residents_number());
    ASSERT_EQ(3, b.get_benefit_residents_number());

    b.delete_resident(&us_emp);
    ASSERT_EQ(0, b.get_usual_residents_number());
    ASSERT_EQ(3, b.get_benefit_residents_number());

    Flat c(18, 12.4, {&us_emp, &us_fill, &ben_emp, &ben_fill});
    ASSERT_EQ(false, c.is_renter_benefit());

    Flat d(18, 12.4, {&ben_emp, &us_fill, &ben_emp, &ben_fill});
    ASSERT_EQ(true, d.is_renter_benefit());

    Flat e;
    e.set_area(15);
    ASSERT_EQ(15, e.get_area());
}

TEST(FlatFunctions, TestExceptions)
{
    Flat a;
    float perc;
    ASSERT_THROW(a.set_area(-1), std::invalid_argument);
    ASSERT_THROW(a.set_area(0), std::invalid_argument);

    ASSERT_THROW(a.is_renter_benefit(), std::logic_error);

    ASSERT_THROW(perc = a.get_benefit_percent(), std::logic_error);

    Resident us_fill("Airat", 2001, male, jobless);
    Benefit_Resident ben_fill("Elena", 1971, female, working, "Student", 10);
    a.add_resident(&us_fill);

    ASSERT_THROW(a.delete_resident(&us_fill), std::logic_error); //Попытка удаления единственного жильца

    a.add_resident(&ben_fill);
    Resident b;
    ASSERT_THROW(a.delete_resident(&b), std::invalid_argument); //попытка неверног удаления
}
