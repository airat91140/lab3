#include "gtest/gtest.h"
#include "vector.h"
#include <string>

using namespace prog;

TEST(VectorConstructors, DefaultConstructor)
{
    vector<int> a;
    ASSERT_EQ(true, a.empty());
    ASSERT_EQ(true, a.begin() == a.end());
}

TEST(VectorConstructors, InitConstructor)
{
    vector <int> a({12, 15, 158, 89});
    ASSERT_EQ(12, a.at(0));
    ASSERT_EQ(15, a.at(1));
    ASSERT_EQ(158, a.at(2));
    ASSERT_EQ(89, a.at(3));
    ASSERT_EQ(false, a.empty());
    ASSERT_EQ(4, a.size());

    vector <int> b({});
    ASSERT_EQ(true, b.empty());
    ASSERT_EQ(true, b.begin() == b.end());
}

TEST(VectorIterConstructor, InitConstructor)
{
    vector <int> a({12, 15, 158, 89});
    auto iter = a.begin();
    ASSERT_EQ(12, *iter++);
    ASSERT_EQ(15, *iter++);
    ASSERT_EQ(158, *iter++);
    ASSERT_EQ(89, *iter++);
    ASSERT_EQ(true, iter == a.end());
}

TEST(VectorConstructors, CopyMoveConstructor)
{
    vector <int> a({12, 15, 158, 89}); // copy constructor
    vector <int> b(a);
    for (auto iter_a = a.begin(), iter_b = b.begin(); iter_a != a.end() && iter_b != b.end(); ++iter_a, ++iter_b) {
        ASSERT_EQ(*iter_a, *iter_b);
    }

    vector <int> c(std::move(a)); // move constructor
    for (auto iter_b = b.begin(), iter_c = c.begin(); iter_b != b.end() && iter_c != c.end(); ++iter_b, ++iter_c) {
        ASSERT_EQ(*iter_b, *iter_c);
    }
}

TEST(VectorFunctions, TestOperators)
{
    vector <int> a({12, 15, 158, 89}), b; // copy assignment operator
    b = a;
    for (auto iter_a = a.begin(), iter_b = b.begin(); iter_a != a.end() && iter_b != b.end(); ++iter_a, ++iter_b) {
        ASSERT_EQ(*iter_a, *iter_b);
    }

    vector <int> c; // move assignment operator
    c = std::move(a);
    for (auto iter_b = b.begin(), iter_c = c.begin(); iter_b != b.end() && iter_c != c.end(); ++iter_b, ++iter_c) {
        ASSERT_EQ(*iter_b, *iter_c);
    }

    vector <int> d({12, 15, 158, 89}); // operator []
    ASSERT_EQ(12, d[0]);
    ASSERT_EQ(15, d[1]);
    ASSERT_EQ(158, d[2]);
    ASSERT_EQ(89, d[3]);

    d[2] = 90;
    ASSERT_EQ(90, d[2]);

}

TEST(VectorIterFunctions, TestOperators)
{
    vector <int> a({12, 15, 158, 89});

    vector<int> :: iterator b(&a[1]), c(&a[1]); // operator ==
    ASSERT_EQ(true, b == c);
    vector<int> :: iterator d(&a[1]), e(&a[2]);
    ASSERT_EQ(false, d == e);

    vector<int> :: iterator f(&a[1]), g(&a[1]); // operator !=
    ASSERT_EQ(false, f != g);
    vector<int> :: iterator h(&a[1]), i(&a[2]);
    ASSERT_EQ(true, h != i);

    vector<int> :: iterator j(&a[1]); // operator *
    ASSERT_EQ(15, *j);

    vector <std::string> A{"abc", "def", "ghj"}; // operator ->
    vector<std::string> :: iterator k(&A[1]);
    ASSERT_EQ(3, k->size());

    vector<int> :: iterator l(&a[1]), m(&a[2]); // prefix operator ++
    ASSERT_EQ(true, ++l == m);
    ASSERT_EQ(true, l == m);

    vector<int> :: iterator n(&a[1]), o(&a[2]); // postfix operator ++
    ASSERT_EQ(false, n++ == o);
    ASSERT_EQ(true, n == o);

    vector<int> :: iterator p(&a[1]), q(&a[2]); // operator +
    ASSERT_EQ(true, p + 1 == q);
    ASSERT_EQ(true, p == q + -1);
}

TEST(VectorFunctions, TestFunctions)
{
    vector <int> a({12, 15, 158, 89});
    vector <int> b({85, 360, 245});
    a.swap(b);
    ASSERT_EQ(12, b.at(0));
    ASSERT_EQ(15, b.at(1));
    ASSERT_EQ(158, b.at(2));
    ASSERT_EQ(89, b.at(3));
    ASSERT_EQ(false, b.empty());
    ASSERT_EQ(4, b.size());

    ASSERT_EQ(85, a.at(0));
    ASSERT_EQ(360, a.at(1));
    ASSERT_EQ(245, a.at(2));
    ASSERT_EQ(false, a.empty());
    ASSERT_EQ(3, a.size());

    vector <int> c({12, 15, 158, 89});
    ASSERT_EQ(12, c.front());
    ASSERT_EQ(89, c.back());

    ASSERT_ANY_THROW(c.at(-1));
    ASSERT_ANY_THROW(c.at(500));
    c.at(1) = 36;
    ASSERT_EQ(36, c.at(1));

    vector <int> d;
    d.push_back(10);
    ASSERT_EQ(10, d[0]);

    vector <int> e({12, 15, 158, 89});
    e.push_back(19);
    ASSERT_EQ(19, e[4]);

    vector <int> f({12, 15, 158, 89});
    f.erase(f.begin() + 1);
    ASSERT_EQ(12, f.at(0));
    ASSERT_EQ(158, f.at(1));
    ASSERT_EQ(89, f.at(2));
    ASSERT_EQ(false, f.empty());
    ASSERT_EQ(3, f.size());
}
