#include "gtest/gtest.h"
#include "resident.h"

TEST(ResidentConstructor, DefaultConstructor)
{
    Resident a;
    ASSERT_EQ(0, a.get_birthdate());
    ASSERT_EQ("", a.get_name());
    ASSERT_EQ(male, a.get_gender());
    ASSERT_EQ(working, a.get_status());
}

TEST(ResidentConstructor, InitNumConstructors)
{
    Resident a("Airat", 2001, male, jobless);
    ASSERT_EQ(2001, a.get_birthdate());
    ASSERT_EQ("Airat", a.get_name());
    ASSERT_EQ(male, a.get_gender());
    ASSERT_EQ(jobless, a.get_status());

    Resident b("Ivanova A. A.", 1945, female, working);
    ASSERT_EQ(1945, b.get_birthdate());
    ASSERT_EQ("Ivanova A. A.", b.get_name());
    ASSERT_EQ(female, b.get_gender());
    ASSERT_EQ(working, b.get_status());

}
TEST(ResidentConstructor, TestException)
{
    ASSERT_THROW(Resident a("", 2001, male, jobless), std::invalid_argument);
    ASSERT_THROW(Resident b("Airat", -1500, male, jobless), std::invalid_argument);
    ASSERT_THROW(Resident c("Airat", 0, male, jobless), std::invalid_argument);
    ASSERT_THROW(Resident a("Airat", 56874, male, jobless), std::invalid_argument);
    ASSERT_THROW(Resident b("", -1500, male, jobless), std::invalid_argument);
}

TEST(ResidentFunctions, TestFunctions)
{
    ASSERT_EQ(false, Resident().is_benefit());
    ASSERT_EQ(false, Resident("Ivanova A. A.", 1945, female, working).is_benefit());

    Resident a, b;
    ASSERT_EQ(true, a == b);

    Resident c("Ivanova A. A.", 1945, female, working), d("Ivanova A. A.", 1945, female, working);
    ASSERT_EQ(true, c == d);

    ASSERT_EQ(false, Resident("Ivanova A. A.", 2001, male, jobless) == Resident("Airat", 2001, male, jobless));
    ASSERT_EQ(false, Resident("Airat", 1945, male, jobless) == Resident("Airat", 2001, male, jobless));
    ASSERT_EQ(false, Resident("Airat", 2001, female, jobless) == Resident("Airat", 2001, male, jobless));
    ASSERT_EQ(false, Resident("Airat", 2001, male, working) == Resident("Airat", 2001, male, jobless));

    Resident e;

    e.set_birthdate(2001);
    ASSERT_EQ(2001, e.get_birthdate());

    e.set_name("Grigoriy");
    ASSERT_EQ("Grigoriy", e.get_name());

    e.set_gender(male);
    ASSERT_EQ(male, e.get_gender());
    e.set_gender(female);
    ASSERT_EQ(female, e.get_gender());

    e.set_status(working);
    ASSERT_EQ(working, e.get_status());
    e.set_status(jobless);
    ASSERT_EQ(jobless, e.get_status());

    ASSERT_EQ(true, e == Resident("Grigoriy", 2001, female, jobless));

    Resident f, g("Airat", 2001, male, jobless);
    ASSERT_EQ(true, f == *f.clone());
    ASSERT_EQ(true, g == *g.clone());

    Resident h, i("Airat", 2001, male, jobless);
    ASSERT_EQ(0, h.get_percent());
    ASSERT_EQ(0, g.get_percent());
}

TEST(ResidentFunctions, TestExceptions)
{
    Resident a, b("Airat", 2001, male, jobless);

    ASSERT_THROW(a.set_birthdate(-10), std::invalid_argument);
    ASSERT_THROW(b.set_birthdate(-10), std::invalid_argument);
    ASSERT_THROW(a.set_birthdate(0), std::invalid_argument);
    ASSERT_THROW(b.set_birthdate(0), std::invalid_argument);
    ASSERT_THROW(a.set_birthdate(55784), std::invalid_argument);
    ASSERT_THROW(b.set_birthdate(78415), std::invalid_argument);

    ASSERT_THROW(a.set_name(""), std::invalid_argument);
    ASSERT_THROW(b.set_name(""), std::invalid_argument);
}

