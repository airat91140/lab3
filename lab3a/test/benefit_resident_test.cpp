#include "gtest/gtest.h"
#include "benefit_resident.h"

TEST(BenefitResidentConstructors, DefaultConstructor)
{
    Benefit_Resident a;
    ASSERT_EQ(0, a.get_birthdate());
    ASSERT_EQ("", a.get_name());
    ASSERT_EQ(male, a.get_gender());
    ASSERT_EQ(working, a.get_status());
    ASSERT_EQ("", a.get_benefit());
    ASSERT_EQ(0, a.get_percent());
}

TEST(BenefitResidentConstructors, InitConstroctor)
{
    Benefit_Resident a("Airat", 2001, male, jobless, "student", 10.6);
    ASSERT_EQ(2001, a.get_birthdate());
    ASSERT_EQ("Airat", a.get_name());
    ASSERT_EQ(male, a.get_gender());
    ASSERT_EQ(jobless, a.get_status());
    ASSERT_EQ("student", a.get_benefit());
    ASSERT_FLOAT_EQ(10.6, a.get_percent());

    Benefit_Resident b("Ivanova", 1932, female, working, "pensioner", 25);
    ASSERT_EQ(1932, b.get_birthdate());
    ASSERT_EQ("Ivanova", b.get_name());
    ASSERT_EQ(female, b.get_gender());
    ASSERT_EQ(working, b.get_status());
    ASSERT_EQ("pensioner", b.get_benefit());
    ASSERT_FLOAT_EQ(25, b.get_percent());
}

TEST(BenefitResidentConstructors, TestException)
{
    ASSERT_THROW(Benefit_Resident("", 2001, male, jobless, "student", 10.6), std::invalid_argument);
    ASSERT_THROW(Benefit_Resident("Airat", -15, male, jobless, "student", 10.6), std::invalid_argument);
    ASSERT_THROW(Benefit_Resident("Airat", 0, male, jobless, "student", 10.6), std::invalid_argument);
    ASSERT_THROW(Benefit_Resident("Airat", 30854, male, jobless, "student", 10.6), std::invalid_argument);
    ASSERT_THROW(Benefit_Resident("Airat", 2001, male, jobless, "", 10.6), std::invalid_argument);
    ASSERT_THROW(Benefit_Resident("Airat", 2001, male, jobless, "student", -5), std::invalid_argument);
    ASSERT_THROW(Benefit_Resident("Airat", 2001, male, jobless, "student", 1000), std::invalid_argument);
}

TEST(BenefitResidentFunctions, TestFunctions)
{
    Benefit_Resident a, b("Airat", 2001, male, jobless, "student", 10.6);
    ASSERT_EQ(true, a.is_benefit());
    ASSERT_EQ(true, b.is_benefit());

    a.set_benefit("student");
    b.set_benefit("student");
    ASSERT_EQ("student", a.get_benefit());
    ASSERT_EQ("student", b.get_benefit());

    a.set_percent(25.8);
    b.set_percent(78.5);
    ASSERT_FLOAT_EQ(25.8, a.get_percent());
    ASSERT_FLOAT_EQ(78.5, b.get_percent());

    Benefit_Resident c, d("Airat", 2001, male, jobless, "student", 10.6);
    ASSERT_EQ(true, a == *a.clone());
    ASSERT_EQ(true, b == *b.clone());
}

TEST(BenefitResidentFunctions, TestExceptions)
{
    Benefit_Resident a, b("Airat", 2001, male, jobless, "student", 10.6);

    ASSERT_THROW(a.set_benefit(""), std::invalid_argument);
    ASSERT_THROW(b.set_benefit(""), std::invalid_argument);

    ASSERT_THROW(a.set_percent(-1), std::invalid_argument);
    ASSERT_THROW(b.set_percent(-1), std::invalid_argument);
    ASSERT_THROW(a.set_percent(110), std::invalid_argument);
    ASSERT_THROW(b.set_percent(156), std::invalid_argument);
}
