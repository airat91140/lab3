#include "gtest/gtest.h"
#include "House.h"
#include "vector.h"

TEST(HouseConstructors, DefaultConstructor)
{
    House a;
    ASSERT_EQ("", a.get_street());
    ASSERT_EQ(0, a.get_number());
    ASSERT_EQ(0, a.get_corpus());
    ASSERT_EQ(0, a.get_meter_cost());
    ASSERT_EQ(true, a.get_person_cost().empty());
}

TEST(HouseFuctions, TestFunctions)
{
    House a;
    Resident us_emp, us_fill("Airat", 2001, male, jobless);
    Benefit_Resident ben_emp, ben_fill("Elena", 1971, female, working, "Student", 10);

    a.set_corpus(10);
    ASSERT_EQ(10, a.get_corpus());

    a.set_meter_cost(10);
    ASSERT_EQ(10, a.get_meter_cost());

    a.set_number(15);
    ASSERT_EQ(15, a.get_number());

    a.set_street("Koshkina");
    ASSERT_EQ("Koshkina", a.get_street());

    Flat flat1(10, 12, {&us_emp, &us_fill, &ben_emp, &ben_fill});
    a.add_flat(flat1);

    ASSERT_EQ(10, a.find_flat(10).get_number());         //Testing adding flat
    ASSERT_DOUBLE_EQ(12, a.find_flat(10).get_area());
    ASSERT_EQ(us_emp, *a.find_flat(10).get_residents()[0]);
    ASSERT_EQ(us_fill, *a.find_flat(10).get_residents()[1]);
    ASSERT_EQ(ben_emp, *a.find_flat(10).get_residents()[2]);
    ASSERT_EQ(ben_fill, *a.find_flat(10).get_residents()[3]);
    ASSERT_EQ(30, a.get_person_cost().at(10));

    Flat flat2(15, 30, {&ben_fill, &us_emp, &us_fill});
    a.add_flat(flat2);

    ASSERT_EQ(15, a.find_flat(15).get_number());         //Testing adding the second flat
    ASSERT_DOUBLE_EQ(30, a.find_flat(15).get_area());
    ASSERT_EQ(ben_fill, *a.find_flat(15).get_residents()[0]);
    ASSERT_EQ(us_emp, *a.find_flat(15).get_residents()[1]);
    ASSERT_EQ(us_fill, *a.find_flat(15).get_residents()[2]);
    ASSERT_NEAR(90, a.get_person_cost().at(15), 1); //0.9 * 30 * 10 / 3

    a.delete_flat(10);
    ASSERT_THROW(a.find_flat(10), std::invalid_argument); //Проверка что не находит
    ASSERT_ANY_THROW(a.get_person_cost().at(10));

    ASSERT_EQ(15, a.find_flat(15).get_number());         //проверка что другая квартриа не удалилась
    ASSERT_DOUBLE_EQ(30, a.find_flat(15).get_area());
    ASSERT_EQ(ben_fill, *a.find_flat(15).get_residents()[0]);
    ASSERT_EQ(us_emp, *a.find_flat(15).get_residents()[1]);
    ASSERT_EQ(us_fill, *a.find_flat(15).get_residents()[2]);
    ASSERT_NEAR(90, a.get_person_cost().at(15), 1); //0.9 * 30 * 10 / 3

    a.add_flat(flat1); //flat1(10, 12, {&us_emp, &us_fill, &ben_emp, &ben_fill});
    a.set_meter_cost(100);
    ASSERT_NEAR(300, a.get_person_cost().at(10), 1); //12 * 100 / 4
    ASSERT_NEAR(900, a.get_person_cost().at(15), 1); //0.9 * 30 * 100 / 3

    a.find_flat(flat1.get_number()).add_resident(&us_emp);
    a.set_flat_sum(flat1.get_number());
    ASSERT_NEAR(240, a.get_person_cost().at(flat1.get_number()), 1); //12 * 100 / 5

    House b;
    a.write_file("test");
    b.read_file("test");
    ASSERT_EQ(a.get_number(), b.get_number());
    ASSERT_EQ(a.get_street(), b.get_street());
    ASSERT_EQ(a.get_corpus(), b.get_corpus());
    ASSERT_EQ(a.get_person_cost(), b.get_person_cost());
    ASSERT_EQ(a.get_meter_cost(), b.get_meter_cost());
    for (auto a_iter = a.get_table().begin(), b_iter = b.get_table().begin(); a_iter != a.get_table().end() && b_iter != b.get_table().end(); ++a_iter, ++b_iter) {
        ASSERT_EQ(a_iter->second.get_number(), b_iter->second.get_number());
        ASSERT_EQ(a_iter->second.get_area(), b_iter->second.get_area());
        prog::vector<Resident *> a_vector = a_iter->second.get_residents(), b_vector = b_iter->second.get_residents();
        for (auto a_res = a_vector.begin(), b_res = b_vector.begin(); a_res != a_vector.end() && b_res != b_vector.begin(); ++a_res, ++b_res) {
            ASSERT_EQ(*(*a_res), *(*b_res));
        }
    }
}

TEST(HouseFuctions, TestExceptions)
{
    House a;

    ASSERT_THROW(a.set_corpus(-10), std::invalid_argument);
    ASSERT_THROW(a.set_corpus(0), std::invalid_argument);

    ASSERT_THROW(a.set_number(-10), std::invalid_argument);
    ASSERT_THROW(a.set_number(0), std::invalid_argument);

    ASSERT_THROW(a.set_street(""), std::invalid_argument);

    ASSERT_THROW(a.set_meter_cost(-10), std::invalid_argument);

    Flat empty;
    ASSERT_THROW(a.add_flat(empty), std::invalid_argument);

    House b;
    Resident us_emp, us_fill("Airat", 2001, male, jobless);
    Benefit_Resident ben_emp, ben_fill("Elena", 1971, female, working, "Student", 10);
    Flat flat1(10, 12, {&us_emp, &us_fill, &ben_emp, &ben_fill});
    Flat flat2(10, 30, {&ben_fill, &us_emp, &us_fill});
    b.add_flat(flat1);

    ASSERT_THROW(b.add_flat(flat2), std::invalid_argument);

    ASSERT_THROW(b.delete_flat(2), std::invalid_argument);

    ASSERT_THROW(b.find_flat(2), std::invalid_argument);
}
