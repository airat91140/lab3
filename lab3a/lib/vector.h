/**
 \file
 \brief Заголовочный файл с описанием шаблонного класса вектора.
 *
 * Данный файл содержит в себе обьявления и реализацию основных методов шаблонного класса, определяющего вектор.
*/
#ifndef VECTOR_H
#define VECTOR_H

#include "vector_iterator.h"
#include <initializer_list>
#include <string_view>
#include <stdexcept>

/// Пространство имен с поределением шаблонного класса вектора и его итератора.
namespace prog {
    /**
     * \brief Шаблонный класс вектора.
     * \tparam arg_type Тип элемента вектора.
     */
    template<class arg_type>
    class vector {
        friend class vector_iterator<arg_type>;

    private:
        arg_type *array_; ///< Массив элементов типа \p arg_type.
        int quota_; ///< Размер памяти, выделенной под массив.
        int size_; ///< Количество элементов в массиве.

    public:
        /**
         * \brief Конструктор вектора по-умолчанию.
         * \details Создает пустой вектор.
         */
        vector() : array_(nullptr), quota_(0), size_(0) {};

        /**
         * \brief Инициализируещий конструктор.
         * \param [in] list Инициализирующий список элементов типа \p arg_type.
         */
        vector(const std::initializer_list<arg_type> &list) : quota_(list.size() / 10 * 10 + 10), size_(list.size()) {
            if (list.size() == 0) {
                array_ = nullptr;
                quota_ = 0;
                return;
            }
            array_ = new arg_type[quota_];
            auto elem = list.begin();
            for (int i = 0; i < size_; ++i, ++elem)
                array_[i] = *elem;
        }

        /**
         * \brief Деструктор вектора.
         */
        ~vector() { delete[] array_; }

        /// Копирующий конструктор.
        vector(const vector &other) : size_(other.size_), quota_(other.quota_), array_(nullptr) {
            if (size_) {
                array_ = new arg_type[quota_];
                for (int i = 0; i < size_; ++i)
                    array_[i] = other.array_[i];
            }
        }

        /// Перемещающий конструктор.
        vector(vector &&other) noexcept: size_(other.size_), quota_(other.quota_), array_(other.array_) {
            other.array_ = nullptr;
            other.size_ = 0;
            other.quota_ = 0;
        };

        /// Копирующий оператор присваивания.
        vector &operator=(const vector &other) {
            if (this != &other) {
                delete[] array_;
                array_ = nullptr;
                quota_ = other.quota_;
                if ((size_ = other.size_) > 0) {
                    array_ = new arg_type[quota_];
                    for (int i = 0; i < size_; ++i)
                        array_[i] = other.array_[i];
                }
            }
            return *this;
        }

        /// Перемещающий оператор присваивания.
        vector &operator=(vector &&other) noexcept {
            if (this != &other) {
                std::swap(size_, other.size_);
                std::swap(quota_, other.quota_);
                std::swap(array_, other.array_);
            }
            return *this;
        }

        /**
         * \brief Функция добавления памяти заранее.
         * \details Функция позволяет заранее добавить необходимую память.
         * Нельзя использовать при уменьшении количесвта выделенной памяти.
         * \param new_capacity Колчиество памяти, выделенной под новые значения.
         */
        void reserve(int new_capacity) {
            if (new_capacity > size_) {
                auto *new_array = new arg_type[new_capacity];
                for (int i = 0; i < size_; ++i)
                    new_array[i] = array_[i];
                delete[] array_;
                array_ = new_array;
                quota_ = new_capacity;
            }
        }

        /**
         * \brief Определние количества элементов вектора.
         * \return Возвращает количество элементов массива.
         */
        [[nodiscard]] inline int size() const { return size_; }

        /// Итератор вектора.
        typedef vector_iterator<arg_type> iterator;

        /**
         * \brief Функция обмена данных двух векторов.
         * \param [in, out] other Другой вектор.
         */
        void swap(vector &other) {
            std::swap(array_, other.array_);
            std::swap(quota_, other.quota_);
            std::swap(size_, other.size_);
        }

        /**
         * \brief Добавление в вектор нового элемента.
         * \details Функция добавляет в конец вектора новый эоемент.
         * @param [in] value Добавляемый элемент.
         */
        void push_back(const arg_type &value) {
            if (size_ < quota_) {
                array_[size_] = value;
                ++size_;
            } else {
                quota_ += 5;
                auto new_array = new arg_type[quota_];
                for (int i = 0; i < size_; ++i)
                    new_array[i] = array_[i];
                new_array[size_] = value;
                ++size_;
                if (array_ != nullptr)
                    delete[] array_;
                array_ = new_array;
            }
        }

        /**
         * \brief Определение является ли вектор пустым.
         * @return Возвращает \p true, если вектор пуст,
         * и \p false в противном.
         */
        [[nodiscard]] bool empty() const noexcept {
            if (size_ <= 0)
                return true;
            return false;
        }

        /**
         * \brief Первый элемент вектора.
         * @return Возвращает первый элемент вектора.
         * Если вектор пуст, то поведение неопределено.
         */
        arg_type &front() { return *array_; }

        /**
         * \Получение элемента по индексу.
         * @param [in] pos позиция элемента.
         * @return Возвращает элемент l-value по заданному индексу.
         * \throws std::out_of_range В случае если происходит выход за границы векторы.
         */
        arg_type &at(int pos) {
            if (pos < 0 || pos >= size_)
                throw std::out_of_range("Invalid position");
            return array_[pos];
        }

        /**
         * \Получение элемента по индексу.
         * @param [in] pos позиция элемента.
         * @return Возвращает элемент r-value по заданному индексу.
         * \throws std::out_of_range В случае если происходит выход за границы векторы.
         */
        const arg_type &at(int pos) const {
            if (pos < 0 || pos >= size_)
                throw std::out_of_range("Invalid position");
            return array_[pos];
        }

        /**
         * \Получение элемента по индексу.
         * @param [in] pos позиция элемента.
         * @return Возвращает элемент l-value по заданному индексу.
         */
        arg_type &operator[](int pos) { return array_[pos]; }

        /**
         * \Получение элемента по индексу.
         * @param [in] pos позиция элемента.
         * @return Возвращает элемент r-value по заданному индексу.
         */
        const arg_type &operator[](int pos) const { return array_[pos]; }

        /**
         * \brief Итератор первого элемента.
         * @return Возвращает итератор на первый эелмент вектора.
         */
        iterator begin() const { return iterator(array_); };

        /**
         * \brief Итератор следующего за последним элементом.
         * @return Возвращает Итератор следующего за последним элементом вектора.
         */
        iterator end() const { return iterator(array_ ? array_ + size_ : nullptr); }

        /**
         * \brief Удаление элемента по заданному индексу.
         * @param [in] pos Заданная позиция.
         * @return Возвращает элемент, следующий за удаленным.
         */
        iterator erase(const iterator &pos) {
            for (int i = &(*pos) - array_; i < size_ && i >= 0; ++i)
                array_[i] = array_[i + 1];
            --size_;
            iterator result(&(*pos) + 1);
            return result;
        }

        /**
         * \brief Восстановление размера вектора.
         * \details Устанавливает количество выделенной памяти на массив равному количеству элементов вектора.
         */
        void shrink_to_fit() {
            if (size_ != quota_) {
                auto new_array = new arg_type[size_];
                for (int i = 0; i < size_; ++i)
                    new_array[i] = array_[i];
                delete[] array_;
                array_ = new_array;
                quota_ = size_;
            }
        }

        /**
         * \brief Последний элемент вектора.
         * @return Возвращает последний элемент вектора.
         * Если вектор пуст, то поведение неопределено.
         */
        arg_type &back() const { return array_[size_ - 1]; }
    };
}

#endif // VECTOR_H
