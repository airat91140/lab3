#include "flat.h"

int Flat::find_resident(const Resident *resident) {
    int index = 0;
    for (auto res : residents) {
        if (*res == *resident)
            return index;
        ++index;
    }
    return -1;
}

Flat::Flat(const Flat &other) : number(other.number), area(other.area) {
    residents.reserve(other.residents.size());
    for (auto res : other.residents)
        residents.push_back(res->clone());
}

Flat::Flat(Flat &&other) noexcept: number(other.number), area(other.area) {
    residents.reserve(other.residents.size());
    for (auto res : other.residents) {
        residents.push_back(res);
    }
    other.residents = {};
    other.number = 0;
    other.area = 0;
}

Flat &Flat::operator=(const Flat &other) {
    if (this != &other) {
        number = other.number;
        area = other.area;
        for (auto res : residents)
            delete res;
        residents.reserve(other.residents.size());
        for (auto res : other.residents)
            residents.push_back(res->clone());
    }
    return *this;
}

Flat &Flat::operator=(Flat &&other) noexcept {
    if (this != &other) {
        std::swap(number, other.number);
        std::swap(area, other.area);
        residents.swap(other.residents);
    }
    return *this;
}

Flat::~Flat() {
    for (auto res : residents)
        delete res;
}

unsigned int Flat::get_residents_number() const {
    return residents.size();
}

void Flat::add_resident(const Resident *o_resident) {
    residents.push_back(o_resident->clone());
}

void Flat::delete_resident(const Resident *o_resident) {
    int index = find_resident(o_resident);
    if (index == -1) {
        throw std::invalid_argument("Invalid resident");
    }
    if (residents.size() == 1)
        throw std::logic_error("Cannot delete last resident");
    residents.erase(residents.begin() + index);
    residents.shrink_to_fit();
}

unsigned int Flat::get_usual_residents_number() const {
    int sum = 0;
    for (auto res : residents)
        if (!res->is_benefit())
            ++sum;
    return sum;
}

unsigned int Flat::get_benefit_residents_number() const {
    int sum = 0;
    for (auto res : residents)
        if (res->is_benefit())
            ++sum;
    return sum;
}

bool Flat::is_renter_benefit() {
    if (residents.empty())
        throw std::logic_error("Residents are not initialized yet");
    return residents.front()->is_benefit();
}

std::ostream &operator<<(std::ostream &out, const Flat &flat) {
    out << "Flat number: " << flat.number << "\nArea: " << flat.area << "\nResidents number: "
        << flat.get_residents_number();
    out << "\nResidents:\n";
    for (auto res : flat.residents)
        out << *res << '\n';
    return out;
}

Flat::Flat(int o_number, double o_area, const prog::vector<Resident *> &o_residents) {
    if (o_number <= 0)
        throw std::invalid_argument("Invalid flat number");
    if (o_area <= 0)
        throw std::invalid_argument("Invalid flat area");
    if (o_residents.empty())
        throw std::invalid_argument("Invalid residents' vector");
    number = o_number;
    area = o_area;
    for (auto o_resident : o_residents)
        residents.push_back(o_resident->clone());
}

float Flat::get_benefit_percent() const {
    if (residents.empty())
        throw std::logic_error("Residents are not initialized yet");
    return residents.at(0)->get_percent();
}

bool Flat::empty() const{
    if (number <= 0 || area <= 0 || residents.empty())
        return true;
    return false;
}

void Flat::set_area(double o_area) {
    if (o_area <= 0)
        throw std::invalid_argument("Invalid area");
    area = o_area;
}
