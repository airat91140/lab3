#include "House.h"
#include <string>

void House::write_file(const std::string &filename) {
    std::ofstream file(filename + ".tb", std::ios::trunc | std::ios::binary);
    if (!file) {
        throw std::invalid_argument("Error happened with file");
    }
    int tmpint;
    tmpint = street.size();
    file.write((char *)&tmpint, sizeof(int));
    file.write(street.c_str(), street.size());
    file.write((char *)&number, sizeof(number));
    file.write((char *)&corpus, sizeof(corpus));
    file.write((char *)&meter_cost, sizeof(meter_cost));
    tmpint = person_cost.size();
    file.write((char *)&tmpint, sizeof(int));
    for (auto pers : person_cost) {
        file.write((char *)&pers.first, sizeof(pers.first));
        file.write((char *)&pers.second, sizeof(pers.second));
    }
    for (const auto& flat : table) {
        file.write((char *)&flat.second.number, sizeof(flat.second.number));
        file.write((char *)&flat.second.area, sizeof(flat.second.area));
        prog::vector<Resident *> residents = flat.second.get_residents();
        tmpint = residents.size();
        file.write((char *)&tmpint, sizeof(int));
        for (auto res : residents) {
            bool tmpbool = res->is_benefit();
            file.write((char *)&tmpbool, 1);
            tmpint = res->name.size();
            file.write((char *)&tmpint, sizeof(int));
            file.write(res->name.c_str(), res->name.size());
            file.write((char *)&res->birthdate, sizeof(res->birthdate));
            file.write((char *)&res->gender, sizeof(male));
            file.write((char *)&res->status, sizeof(working));
            if (res->is_benefit()) {
                file.write((char *)&dynamic_cast<Benefit_Resident*>(res)->percent, sizeof(float));
                tmpint = dynamic_cast<Benefit_Resident*>(res)->benefit.size();
                file.write((char *)&tmpint, sizeof(int));
                file.write((char *)dynamic_cast<Benefit_Resident*>(res)->benefit.c_str(), dynamic_cast<Benefit_Resident*>(res)->benefit.size());
            }
        }
    }
}

void House::read_file(const std::string &filename) {
    std::ifstream file(filename + ".tb", std::ios::binary);
    char buf[150];
    int tmpint;
    if(!file)
        throw std::invalid_argument("Error happened with file");
    person_cost.clear();
    table.clear();
    file.read((char *)&tmpint, sizeof(int));
    file.read(buf, tmpint);
    buf[tmpint] = 0;
    street = buf;
    file.read((char *)&number, sizeof(number));
    file.read((char *)&corpus, sizeof(corpus));
    file.read((char *)&meter_cost, sizeof(meter_cost));
    int person_cost_size;
    file.read((char *)&person_cost_size, sizeof(int));
    for (int i = 0; i < person_cost_size; ++i) {
        int num, price;
        file.read((char *)&num, sizeof(num));
        file.read((char *)&price, sizeof(price));
        person_cost.insert({num, price});
    }
    for (int i = 0; i < person_cost_size; ++i) {
        Flat flat;
        file.read((char *)&flat.number, sizeof(Flat::number));
        file.read((char *)&flat.area, sizeof(Flat::area));

        int residents_size;
        file.read((char *)&residents_size, sizeof(int));
        for (int j = 0; j < residents_size; ++j) {
            bool type;
            file.read((char *)&type, 1);
            if (type) {
                Benefit_Resident resident;
                file.read((char *)&tmpint, sizeof(int));
                file.read(buf, tmpint);
                buf[tmpint] = 0;
                resident.name = buf;
                file.read((char *)&resident.birthdate, sizeof(resident.birthdate));
                file.read((char *)&resident.gender, sizeof(resident.gender));
                file.read((char *)&resident.status, sizeof(resident.status));
                file.read((char *)&resident.percent, sizeof(resident.percent));
                file.read((char *)&tmpint, sizeof(int));
                file.read(buf, tmpint);
                resident.benefit = buf;
                buf[tmpint] = 0;
                flat.add_resident(&resident);
            }
            else {
                Resident resident;
                file.read((char *)&tmpint, sizeof(int));
                file.read(buf, tmpint);
                buf[tmpint] = 0;
                resident.name = buf;
                file.read((char *)&resident.birthdate, sizeof(resident.birthdate));
                file.read((char *)&resident.gender, sizeof(resident.gender));
                file.read((char *)&resident.status, sizeof(resident.status));
                flat.add_resident(&resident);
            }
        }
        add_flat(flat);
    }
}

void House::set_corpus(int o_corpus) {
    if (o_corpus <= 0)
        throw std::invalid_argument("Invalid corpus");
    corpus = o_corpus;
}

void House::add_flat(const Flat &flat) {
    if (flat.empty())
        throw std::invalid_argument("Invalid Flat");
    auto error = table.insert({flat.get_number(), flat});
    if (!error.second)
        throw std::invalid_argument("Invalid key");
    person_cost.insert({flat.get_number(), get_flat_sum(flat) / flat.get_residents_number()});
}

void House::delete_flat(int o_number) {
    int error = table.erase(o_number);
    if (error < 1)
        throw std::invalid_argument("Invalid flat number");
    person_cost.erase(o_number);
}

Flat &House::find_flat(int o_number) {
    auto iter = table.find(o_number);
    if (table.end() == iter)
        throw std::invalid_argument("Invalid Flat number");
    return iter->second;
}

void House::set_street(const std::string &o_street) {
    if(o_street.empty())
        throw std::invalid_argument("Invalid street name");
    street = o_street;
}

void House::set_number(int o_number) {
    if (o_number <= 0)
        throw std::invalid_argument("Invalid house number");
    number = o_number;
}

void House::set_meter_cost(int o_meter_cost) {
    if (o_meter_cost < 0)
        throw std::invalid_argument("Invalid meter cost");
    meter_cost = o_meter_cost;
    fix_person_cost();
}

std::ostream &operator<<(std::ostream &out, const House &house) {
    out << "House:\nStreet: " << house.street << "\nNumber: " << house.number;
    out << "\nCorpus: " << house.corpus << "\nMeter cost: " << house.meter_cost << '\n' << '\n';
    for (const auto& flat_iter : house.table) {
        out << flat_iter.second << "Price for person: " << house.person_cost.at(flat_iter.first) << "\n\n";
    }
    return out;
}

void House::fix_person_cost() {
    person_cost.clear();
    for (const auto& table_iter : table) {
        person_cost.insert({table_iter.first, get_flat_sum(table_iter.second) / table_iter.second.get_residents_number()});
    }
}

int House::get_flat_sum(const Flat &flat) const {
    return (int)(flat.get_area() * (1 - flat.get_benefit_percent() / 100)  * meter_cost);
}

void House::set_flat_sum(int number_o) {
    Flat flat = find_flat(number_o);
    person_cost[number_o] = get_flat_sum(flat) / flat.get_residents_number();
}
