#include "resident.h"

std::ostream &operator<<(std::ostream &out, Resident &resident) {
    return resident.print(out);
}

Resident::Resident(const std::string &o_name, int o_birthdate, Gender o_gender, Status o_status) {
    time_t now;
    time(&now);
    tm *ts = localtime(&now);
    if (o_birthdate <= 0 || o_birthdate > ts->tm_year + 1900)
        throw std::invalid_argument("Invalid birthdate");
    if (o_name.empty())
        throw std::invalid_argument("Invalid name");
    name = o_name;
    birthdate = o_birthdate;
    gender = o_gender;
    status = o_status;
}

bool Resident::operator==(const Resident &other) const {
    if (this->name == other.name &&
        this->birthdate == other.birthdate &&
        this->gender == other.gender &&
        this->status == other.status &&
        this->is_benefit() == other.is_benefit())
        return true;
    else
        return false;
}

void Resident::set_birthdate(int o_birthdate) {
    time_t now;
    time(&now);
    tm *ts = localtime(&now);
    if (o_birthdate <= 0 || o_birthdate > ts->tm_year + 1900)
        throw std::invalid_argument("Invalid birthdate");
    birthdate = o_birthdate;
}

void Resident::set_name(const std::string &o_name) {
    if (o_name.empty())
        throw std::invalid_argument("Invalid name");
    name = o_name;
}

Resident *Resident::clone() const {
    return new Resident(*this);
}

std::ostream &Resident::print(std::ostream &out) {
    out << "Name: " << name << "\nBirthdate: " << birthdate;
    out << "\nGender: " << (gender == 0 ? "male" : "female") << "\nStatus:";
    out << (status == 0 ? "working" : "jobless");
    return out;
}
