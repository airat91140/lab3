/**
 \file
 \brief Заголовочный файл с описанием класса квартиры.
 *
 * Данный файл содержит в себе обьявления методов класса, определяющего квартиру.
*/
#ifndef LAB3_FLAT_H
#define LAB3_FLAT_H

#include "vector.h"
#include "resident.h"
#include "benefit_resident.h"

///Класс, определяющий квартиру
class Flat {
    friend class House;

private:
    int number; ///< Номер квартиры.
    double area; ///< Площадь квартиры.
    prog::vector<Resident *> residents; ///Массив указателей на жильцов квартиры.

    /**
     * \brief Вспомогательный мето для поиска жильца.
     * \param [in] resident Житель, которого надо найти
     * \return Возвращает индекс жителя в векторе \p residents
     * или, если жилец не найден, то возвращает -1
     */
    int find_resident(const Resident *resident);

public:

    /**
     * \brief Метод определяющий корректность квартиры.
     * \details Проверяет, не находятся ли параметры квартиры в значениях по-умолчанию.
     * \return Возвращает \p true, если хоть один парметр квартиры находится в состоянии по-умолчанию,
     * и \p false в противном случае.
     */
    [[nodiscard]] bool empty() const;

    /**
     * \brief Конструктор класса по умолчанию
     * \details Создает класс со значениями по умолчанию:
     * Номер квартиры: 0,
     * Площадь: 0,
     * Массив жителей: {}.
     */
    Flat() : number(0), area(0), residents() {}

    /**
     * \brief Инициализируещий конструктор.
     * \param [in] number Номер квартиры.
     * \param [in] area Площадь квартиры
     * \param [in] residents Вектор указателей на жильцов квартиры. Первый жилец в векторе должен быть сьемщиком.
     * \throws std::invalid_argument В случае отрицательного значения параметра \p number.
     * \throws std::invalid_argument В случае отрицательного значения параметра \p area.
     * \throws std::invalid_argument В случае передаче пустого вектора в поле \p residents.
     */
    Flat(int number, double area, const prog::vector<Resident *> &residents);

    ///Копирующий конструктор.
    Flat(const Flat &flat);

    ///Перемещающий конструктор.
    Flat(Flat &&) noexcept;

    ///Копирующий оператор присваивания.
    Flat &operator=(const Flat &);

    ///Перемещающий оператор присваивания.
    Flat &operator=(Flat &&) noexcept;

    ///Деструктор
    ~Flat();

    /**
     * \brief Перегруженный оператор вывода информации о квартире.
     * \param [in, out] out Поток, в который надо вывести информацию.
     * \param [in] flat Квартира, информация о которой надо вывести.
     * \returns Возвращает поток, с добавленной информацией о квартире.
     */
    friend std::ostream &operator<<(std::ostream &out, const Flat &flat);

    /**
     * \brief Получение количества жильцов.
     * \details Метод позволяет получить точное количество жильцов,
     * проживающих в квартире.
     * \return Возвращает число жильцов типа \p int.
     */
    [[nodiscard]] unsigned int get_residents_number() const;

    /**
     * \brief Геттер площади квартиры.
     * \return Возвращает значение площди квартиры типа \p double.
     */
    [[nodiscard]] inline double get_area() const { return area; };

    /**
     * \brief Сеттер площади.
     * \param [in] area Площадь квартиры.
     * \throws std::invalid_argument В случае передачи в качестве \p area отрицательного значения.
     */
    void set_area(double area);

    /**
     * \brief Добавление нового жителя.
     * \details Метод позволяет заселить в квартиру нового жителя.
     * При нахождении квартиры в доме после добавления надо использовать set_flat_sum(number) для корректировке цены.
     * \param [in] resident Житель, которого нужно добавить в квартиру.
     */
    void add_resident(const Resident *resident);

    /**
     * \brief Удаление жильца из квартиры.
     * \details Метод позволяет удалить заданного жильца из квартиры.
     * \param [in] resident Жилец, которого надо удалить.
     * Нет необходимости передавать указатель на жильца именно из вектора.
     * Можно передать жильца типа #Resident, с параметрами, равными параметрам удаляемого.
     * При нахождении квартиры в доме после добавления надо использовать set_flat_sum(number) для корректировке цены.
     * \throws std::invalid_error В случае отсутствия удаляемого элемента в векторе.
     * \throws std::logic_error В случае удаления последнего жильца из квартиры.
     */
    void delete_resident(const Resident *resident);

    /**
     * \brief Получение количества обычных жильцов в квартире.
     * \return Возвращает число обычных жильцов типа \parblock unsigned int \endparblock
     */
    [[nodiscard]] unsigned int get_usual_residents_number() const;

    /**
     * \brief Получение количества льготных жильцов в квартире.
     * \return Возвращает число льготных жильцов типа \parblock unsigned int \endparblock
     */
    [[nodiscard]] unsigned int get_benefit_residents_number() const;

    /**
     * \brief Определение льготности съемщика квартиры.
     * \return Возвращает \p true, если съемщик льготный, и \p false в противном случае.
     */
    bool is_renter_benefit();

    /**
     * \brief Определение процента скидки для квартиры.
     * \return Возвращает процент скидки в процентах типа \p float.
     */
    [[nodiscard]] float get_benefit_percent() const;

    /**
     * \brief Геттер для номера квартиры.
     * \return Возвращает номер квартиры типа \p int.
     */
    [[nodiscard]] inline int get_number() const { return number; }

    /**
     * \brief Геттер вектора жильцов квартиры.
     * \return Возвращает ссылку на константный вектор жильцов квартиры.
     */
    [[nodiscard]] const prog::vector<Resident *> &get_residents() const { return residents; };

};


#endif //LAB3_FLAT_H
