#include "benefit_resident.h"

void Benefit_Resident::set_percent(float perc) {
    if (perc >= 0 && perc <= 100)
        percent = perc;
    else {
        throw std::invalid_argument("Invalid percentage");
    }
}

std::ostream &operator<<(std::ostream &out, Benefit_Resident &resident) {
    return resident.print(out);
}

Benefit_Resident::Benefit_Resident(const std::string &o_name, int o_birthdate, Gender o_gender, Status o_status,
                                   const std::string &o_benefit, float o_percent)
        : Resident(o_name, o_birthdate, o_gender, o_status) {
    if (o_benefit.empty())
        throw std::invalid_argument("Invalid string");
    if (o_percent < 0 || o_percent > 100)
        throw std::invalid_argument("Invalid percent");
    benefit = o_benefit;
    percent = o_percent;
}

void Benefit_Resident::set_benefit(const std::string &o_benefit) {
    if (o_benefit.empty())
        throw std::invalid_argument("Invalid string");
    benefit = o_benefit;
}

Benefit_Resident *Benefit_Resident::clone() const{
    return new Benefit_Resident(*this);
}

std::ostream &Benefit_Resident::print(std::ostream &out) {
    out << "Name: " << get_name() << "\nBirthdate: " << get_birthdate();
    out << "\nGender: " << (get_gender() == 0 ? "male" : "female") << "\nStatus: ";
    out << (get_status() == 0 ? "working" : "jobless");
    out << "\nBenefit: " << benefit << "\nPercent: " << percent;
    return out;
}
