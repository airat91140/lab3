#include "house_ui.h"
#include "ui_house_ui.h"
#include <QString>
#include <sstream>
#include "mainwindow.h"
#include <QMessageBox>

house_ui::house_ui(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::house_ui)
{
    ui->setupUi(this);
    house = nullptr;
}

house_ui::house_ui(House &house, QWidget *parent) :  QDialog(parent) {
    ui = new Ui::house_ui;
    ui->setupUi(this);
    ui->StreetLineEdit->setText(QString::fromUtf8(house.get_street().c_str()));
    ui->CorpusSpinBox->setValue(house.get_corpus());
    ui->MeterCostSpinBox->setValue(house.get_meter_cost());
    ui->NumberSpinBox->setValue(house.get_number());
    std::stringstream house_info;
    house_info << house;
    ui->textBrowser->setText(QString::fromStdString(house_info.str()));
    this->house = &house;
}


house_ui::~house_ui()
{
    delete ui;
}

void house_ui::on_SaveButton_clicked()
{
    try {
        house->set_corpus(ui->CorpusSpinBox->value());
        house->set_number(ui->NumberSpinBox->value());
        house->set_meter_cost(ui->MeterCostSpinBox->value());
        house->set_street(ui->StreetLineEdit->text().toStdString());
        this->close();
        delete this;
    } catch (std::exception &exc) {
        QMessageBox alert(QMessageBox::Warning, "", exc.what());
        alert.exec();
    }
}
