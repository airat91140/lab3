#ifndef RESIDENT_NEW_H
#define RESIDENT_NEW_H

#include <QDialog>
#include "House.h"
#include "mainwindow.h"

namespace Ui {
class Resident_new;
}

class Resident_new : public QDialog
{
    Q_OBJECT

public:
    explicit Resident_new(MainWindow * = nullptr, House * = nullptr);
    ~Resident_new();

private slots:
    void on_SaveButton_clicked();

private:
    Ui::Resident_new *ui;
    House *house;
    MainWindow *parent;
};

#endif // RESIDENT_NEW_H
