#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include "House.h"
#include "vector.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    friend class Resident_new;
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    House house;
    void ShowFlats();
    void ShowResidents();

private slots:
    void on_actionInfo_triggered();

    void on_AddFlatButton_clicked();

    void on_FlatListWidget_itemClicked(QListWidgetItem *item);

    void on_ResidentListWidget_clicked(const QModelIndex &index);

    void on_AddResidentButton_clicked();

    void on_SaveButton_clicked();

    void on_DeleteButton_clicked();

    void on_FlatListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_actionSave_as_triggered();

    void on_actionOpen_triggered();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
