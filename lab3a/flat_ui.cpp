#include "flat_ui.h"
#include "ui_flat_ui.h"
#include <sstream>
#include <QMessageBox>

flat_ui::flat_ui(Flat *flat, MainWindow *parent) :
    QDialog(parent),
    ui(new Ui::flat_ui),
    flat(flat),
    parent(parent)
{
    ui->setupUi(this);
    if (flat != nullptr) {
        ui->AreaDoubleSpinBox->setValue(flat->get_area());
        std::stringstream flat_info;
        flat_info << *flat;
        ui->textBrowser->setText(QString::fromStdString(flat_info.str()));
    }
}

flat_ui::~flat_ui()
{
    delete ui;
}

void flat_ui::on_pushButton_clicked()
{
    try {
        flat->set_area(ui->AreaDoubleSpinBox->value());
        this->close();
        delete this;
    }
    catch (std::exception &exc) {
        QMessageBox alert(QMessageBox::Warning, "", exc.what());
        alert.exec();
    }
}

void flat_ui::on_DeleteButton_clicked()
{
    try {
        parent->house.delete_flat(flat->get_number());
        this->close();
        parent->ShowFlats();
        parent->ShowResidents();
        delete this;
    }
    catch (std::exception &exc) {
        QMessageBox alert(QMessageBox::Warning, "", exc.what());
        alert.exec();
    }
}
